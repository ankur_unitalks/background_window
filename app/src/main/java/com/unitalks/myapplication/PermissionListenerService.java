package com.unitalks.myapplication;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import java.util.List;
import java.util.SortedMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

public class PermissionListenerService extends Service {


    private Timer timer;

    private long displayPermissionTimeout;

    @Override
    public void onCreate() {
        super.onCreate();

        try {

            displayPermissionTimeout = 2 * 1000; // 10 Minutes
            timer = new Timer();

            // notification builder
            NotificationCompat.Builder notificationBuilder;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

                String name="Testing";
                NotificationChannel channel = new NotificationChannel("permission_notification_channel",
                        name,
                        NotificationManager.IMPORTANCE_DEFAULT);

                ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

                notificationBuilder = new NotificationCompat.Builder(this, "permission_notification_channel");
            } else {
                notificationBuilder = new NotificationCompat.Builder(this);
            }

            String name="TimeoutIQ";

            notificationBuilder
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentText(name)
                    .setContentTitle("TimeoutIQ")
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .setOngoing(true);

            notificationBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);

            Notification notification= notificationBuilder.build();

            startForeground(3, notification);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    int lastRunningProcessCount=0;


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {

        if (intent!=null){
            if (intent.getAction()!=null && intent.getAction().equals("stop")) {
                killPermissionService();
                return START_STICKY;
            }
        }


        return START_STICKY;
    }




    private void killPermissionService(){
        if (timer!=null)
            timer.cancel();
        stopForeground(true);
        stopSelf();

    }

    @Override
    public void onDestroy() {
         super.onDestroy();
    }



}
