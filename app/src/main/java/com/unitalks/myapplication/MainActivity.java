package com.unitalks.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.List;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initiateDialog();
        firebaseToken();

        if (!isMyServiceRunning(AppController.getContext(), PermissionListenerService.class)) {
            Intent permissionIntent=new Intent(MainActivity.this, PermissionListenerService.class);
            ContextCompat.startForegroundService(MainActivity.this,permissionIntent);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter intentFilter=new IntentFilter();
        intentFilter.addAction("SWITCH_CHILD_SUCCESSFULLY");
        LocalBroadcastManager.getInstance(AppController.getContext()).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(AppController.getContext()).registerReceiver(mMessageReceiver,
                intentFilter);

    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
      //  LocalBroadcastManager.getInstance(AppController.getContext()).unregisterReceiver(mMessageReceiver);
    }



    private void firebaseToken(){

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        Log.e("TOken",task.getResult().getToken());
                    }
                });

    }


    /**
     * Broadcast Receiver
     */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equalsIgnoreCase("SWITCH_CHILD_SUCCESSFULLY")){
                Log.e("onmessagereceived","dsfa");
                showDialog();
            }
        }
    };

    private androidx.appcompat.app.AlertDialog.Builder dialogBuilder;

    private androidx.appcompat.app.AlertDialog dialog;


    private void initiateDialog() {
        // initialize only when the service is not running

            dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(new ContextThemeWrapper(AppController.getContext(), R.style.AppFullScreenTheme));
            // dialogBuilder.setTitle(R.string.app_name);
            LayoutInflater inflater2 = (LayoutInflater) AppController.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater2.inflate(R.layout.questoinnaire, null);
            dialogBuilder.setView(dialogView);


            dialog = dialogBuilder.create();
             final WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            final Window dialogWindow = dialog.getWindow();
            if (dialogWindow != null) {
                dialogWindow.setAttributes(layoutParams);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    dialogWindow.setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
                } else {

                    dialogWindow.setType(WindowManager.LayoutParams.TYPE_PHONE);

                }
            }
    }


    public void showDialog() {
        try {
                 if (dialog != null && dialog.isShowing()) {
                    //show no questionnaire when previously question is not answer
                    return;
                }


                if (dialog == null) {
                    return;
                }
                if (!dialog.isShowing()) {
                    dialog.show();
                    if (dialog.getWindow() != null) {
                        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    }
                }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
